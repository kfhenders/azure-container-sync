﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Configuration;

namespace Kfh.AzSync.Config
{
    public class BlobContainerSettings
    {
        static object _lockObj = new object();
        static BlobContainerSettings _current = null;

        private BlobContainerSettings() { }

        public static BlobContainerSettings Current
        {
            get
            {
                if (_current == null)
                {
                    lock (_lockObj)
                    {
                        if (_current == null)
                        {
                            _current = JsonConfig.ReadConfig<BlobContainerSettings>(ConfigurationManager.AppSettings["BlobContainerSettings"]);
                        }
                    }
                }
                return _current;
            }
        }

//{
//    "Source": {
//        "ConnectionString": "",
//        "BlobContainer": "",
//        "SyncLogContainer": "synclogs"
//    },
//    "Dest": {
//        "ConnectionString": "",
//        "BlobContainer": "",
//        "SyncLogContainer": "synclogs"
//    }
//}

        public Container Source { get; set; }
        public Container Dest { get; set; }
    }

    public class Container
    {
        public string ConnectionString { get; set; }
        public string BlobContainer { get; set; }
        public string SyncLogContainer { get; set; }
    }
}
