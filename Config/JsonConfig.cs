﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace Kfh.AzSync.Config
{
    public static class JsonConfig
    {
        static string _executionDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public static T ReadConfig<T>(string configFile)
        {
            string configJson;
            string path = Path.Combine(_executionDir, configFile);
            using (StreamReader sr = new StreamReader(path))
            {
                configJson = sr.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<T>(configJson);
        }
    }
}
