﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace Kfh.AzSync
{

    public partial class Synchronizer
    {
        // Log File details
        static DateTime _logFileDate;
        static string _logDir = ConfigurationManager.AppSettings["LogDir"];
        static string _logFileFormat = Path.Combine(_logDir, "AzSync.{0}.log");
        static string _logFileName;
        static bool _logNoOPs = bool.Parse(ConfigurationManager.AppSettings["LogNoOPs"]);
        static string _errorLogFileFormat = Path.Combine(_logDir, "AzSync.{0}.Exceptions.log");
        static string _errorLogFileName;
        static string _storageExceptionFileFormat = Path.Combine(_logDir, "AzSync.{0}.StorageExceptions.log");
        static string _passDetailsLogFileFormat = Path.Combine(_logDir, "AzSync.{0}.PassDetails.log");
        static string _passDetailsLogFileName;

        // Working Program Data details
        static string _programDataDir = ConfigurationManager.AppSettings["ProgramDataDir"];
        static string _contentMD5FilePath = Path.Combine(_programDataDir, "ContentMD5s.txt");
        static string _contentMD5ZipFileName = "ContentMD5s.zip";
        static string _contentMD5ZipFilePath = Path.Combine(_programDataDir, _contentMD5ZipFileName);
        static string _continuationTokenFileName = "ContinuationToken.json";
        static string _continuationTokenFilePath = Path.Combine(_programDataDir, _continuationTokenFileName);
        static long _blobsInErrorStateLoaded = 0;
        static string _blobsInErrorStateFilePath = Path.Combine(_programDataDir, "BlobsInErrorState.txt");
        static string _blobsInErrorStateZipFileName = "BlobsInErrorState.zip";
        static string _blobsInErrorStateZipFilePath = Path.Combine(_programDataDir, _blobsInErrorStateZipFileName);

        static int _estmatedThreadCount = int.Parse(ConfigurationManager.AppSettings["EstimatedThreadCount"]);
        static int _estimatedAssetCount = int.Parse(ConfigurationManager.AppSettings["EstimatedAssetCountMillions"]) * 1000000;
        static int _stateLogIntervalMinutes = int.Parse(ConfigurationManager.AppSettings["StateSaveInterValMinutes"]);
        static int _errorRetryWaitMinutes = int.Parse(ConfigurationManager.AppSettings["ErrorRetryWaitMinutes"]);
        static int _errorThreshold = int.Parse(ConfigurationManager.AppSettings["ErrorThreshold"]);
        static DateTime _nextStateLogTime = DateTime.MaxValue;

        static ConcurrentDictionary<string, string> _contentMD5sDict = new ConcurrentDictionary<string, string>(_estmatedThreadCount, _estimatedAssetCount);
        static BlobContinuationToken _continuationToken = null;
        static CancellationTokenSource _cancelTokenSource = new CancellationTokenSource();

        static int _uploadLogFilesCalled = 0;
        static ConcurrentDictionary<string, bool> _uploadedLogFiles = null;

        /// <summary>
        /// Dictionary used to hold blobs for which an error has ocurred. Key is id, value is the next time we can retry it.
        /// Intentionally NOT using a concurrent Dictionary as I want to control the locking for reads and Writes.
        /// Original implementation used a ConcurrentQueue and in memory cache, but could't keep them in sync and the 
        /// same id was being added multiple times to the queue. 
        /// </summary>
        static Dictionary<string, DateTime> _blobsInErrorStateDict = new Dictionary<string, DateTime>();
        static object _blobsInErrorStateLockObj = new object();

        #region public members

        /// <summary>
        /// Empty Constructor for Topshelf to Create instance
        /// </summary>
        public Synchronizer()
        {
            // No exceptions from this method
            try
            {
                if (!Directory.Exists(_programDataDir))
                {
                    Directory.CreateDirectory(_programDataDir);
                }
                if (!Directory.Exists(_logDir))
                {
                    Directory.CreateDirectory(_logDir);
                }
            }
            catch { }
        }

        /// <summary>
        /// Method that gets run when the service is started
        /// </summary>
        public void Start()
        {
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.DefaultConnectionLimit = 1000;
            try
            {
                SetLogFileNames();
                Task.Run(async () => await Synchronize().ConfigureAwait(false));
                Task.Run(async () => await RetryBlobsInErrorState().ConfigureAwait(false));
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method Start()", ex);
                throw;
            }
        }

        
        /// <summary>
        /// Method that gets run when the service is stopped
        /// </summary>
        public void Stop()
        {
            Log("Service Stopping ...");
            _cancelTokenSource.Cancel(false);
            // Sleep a bit to let everything cancel out
            Thread.Sleep(5000);
        }

        /// <summary>
        /// Method that gets run when the server is shutting down
        /// </summary>
        public void Shutdown()
        {
            Log("Shutting Down ...");
            _cancelTokenSource.Cancel(false);
            // Sleep a bit to let everything cancel out
            Thread.Sleep(5000);
        }

        #endregion

        #region private members

        private async Task Synchronize()
        {

            Log("Service Started ...");
            await SetMetadata();
            TryLoadState();
            _nextStateLogTime = DateTime.UtcNow.AddMinutes(_stateLogIntervalMinutes);

            bool cancelled = false;
            while (true)
            {
                try
                {
                    PassDetails.StartTimeUtc = DateTime.UtcNow;
                    PassDetails.Type = _continuationToken == null ? PassType.Full : PassType.Partial;
                    PassDetails.State = PassState.InProgress;
                    Log(String.Format("{0} Pass {1}", PassDetails.Type == PassType.Full ? "Starting Full" : "Continuing", PassDetails.Name));
                    
                    BlobResultSegment resultSegment = await AzBlobContainers.Current.ListBlobsAsync(ContainerType.SourceBlob, _continuationToken, _cancelTokenSource.Token).ConfigureAwait(false);
                    _continuationToken = resultSegment.ContinuationToken;

                    while (resultSegment != null && resultSegment.ContinuationToken != null)
                    {
                        var resultSegmentAsync = AzBlobContainers.Current.ListBlobsAsync(ContainerType.SourceBlob, _continuationToken, _cancelTokenSource.Token).ConfigureAwait(false);
                        ProcessResultSegment(resultSegment);
                        SaveState(DateTime.UtcNow);
                        
                        if (_cancelTokenSource.IsCancellationRequested)
                        {
                            break;
                        }

                        resultSegment = await resultSegmentAsync;
                        _continuationToken = resultSegment.ContinuationToken;

                    }
                    // Need to finish the last set of results
                    ProcessResultSegment(resultSegment);
                    ProcessEndOfPass(DateTime.UtcNow);
                }
                catch (TaskCanceledException tce)
                {
                    if (!_cancelTokenSource.IsCancellationRequested)
                    {
                        LogError("TaskCanceledException caught in method Synchronize", tce);
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception caught in method Synchronize", ex);
                }
                finally
                {
                    if (_cancelTokenSource.IsCancellationRequested)
                    {
                        cancelled = true;
                    }
                }
                if (cancelled)
                {
                    break;
                }
            }
            Log("Exiting method Synchronize");
        }

        private void ProcessResultSegment(BlobResultSegment resultSegment)
        {
            // Let Exceptions bubble out to calling method
            if (resultSegment == null || resultSegment.Results == null || resultSegment.Results.Count() == 0) return;

            IEnumerable<CloudBlockBlob> blobs =
                (from b in
                     resultSegment.Results
                 select b as CloudBlockBlob);

            SynchronizeBlobs(blobs);

        }

        private void SynchronizeBlobs(IEnumerable<CloudBlockBlob> blobs)
        {
            
            // Let Exceptions bubble out to calling method
            Task[] tasks = new Task[blobs.Count()];
            int index = 0;
            foreach (CloudBlockBlob blob in blobs)
            {
                int currIndex = index;
                tasks[currIndex] = Task.Run(async () => await SynchronizeBlob(blob.Name, blob.Properties.ContentMD5).ConfigureAwait(false));
                index++;
            }
            Task.WaitAll(tasks);
            
        }

        private async Task<BlobAction> SynchronizeBlob(string id, string sourceContentMD5, bool errorRetry = false)
        {

            CloudBlob destBlob;
            BlobAction action = BlobAction.NoOP;
            string copyId = null;
            bool inErrorState = true;
            try
            {
                if (errorRetry)
                {
                    PassDetails.IncrementErrorRetryCount();
                }
                else
                {
                    PassDetails.IncrementProcessedCount();
                }
                
                // First check if the Destination MD5 is in the _contentMD5sDict and matches the source MD5
                // If that's the case, we don't need to do anything
                if (_contentMD5sDict.ContainsKey(id) && _contentMD5sDict[id] == sourceContentMD5)
                {
                    inErrorState = false;
                    if (errorRetry)
                    {
                        lock (_blobsInErrorStateLockObj)
                        {
                            _blobsInErrorStateDict.Remove(id);
                        }
                    }
                    return action;
                }

                // Need to get the destination blob and check the MD5, only a server blob will have the MD5
                string destContentMD5 = null;
                destBlob = await AzBlobContainers.Current.GetCloudBlockBlobAsync(ContainerType.DestBlob, id, _cancelTokenSource.Token).ConfigureAwait(false);

                if (destBlob.IsFromServer && !String.IsNullOrEmpty(destBlob.Blob.Properties.ContentMD5))
                {
                    destContentMD5 = destBlob.Blob.Properties.ContentMD5;
                }
                if (destContentMD5 == sourceContentMD5)
                {
                    // They're equal, save it in the Dictionary for next time
                    _contentMD5sDict[id] = destContentMD5;
                    inErrorState = false;
                    if (errorRetry)
                    {
                        lock (_blobsInErrorStateLockObj)
                        {
                            _blobsInErrorStateDict.Remove(id);
                        }
                    }
                    return action;
                }
                
                // Add a null entry to the _contentMD5sDict so we get an Accurate count of the Source blobs
                _contentMD5sDict[id] = null;


                // If we got this far, we need to do a copy. It will be one of the following cases:
                // Add: The destBlob is not from the server meaning it does not exist
                // Repair: It's on the server, but there is a pending Copy that has not completed successfully
                // Update: Exists on the server and State is OK, just need to update it.
                // Pending: Even though we tried to abort a Pending copy operation, somehow we still get a 
                //          (409) Conflict: PendingCopyOperation exception. 
                //          We'll call this Pending and it will get checked again on the next pass
                if (!destBlob.IsFromServer)
                {
                    action = BlobAction.Add;
                }
                else if (destBlob.Blob.CopyState != null && destBlob.Blob.CopyState.Status == CopyStatus.Pending)
                {
                    // Try and abort the existing Copy.
                    // Sometimes it don't work and somehow we get a (409) Conflict: NoPendingCopyOperation exception
                    bool aborted = await AzBlobContainers.Current.AbortCopyAsync(destBlob.Blob, destBlob.Blob.CopyState.CopyId, _cancelTokenSource.Token).ConfigureAwait(false);
                    if (aborted)
                    {
                        action = BlobAction.Repair;
                    }
                    else
                    {
                        action = BlobAction.Update;
                    }
                }
                else
                {
                    action = BlobAction.Update;
                }
                 
                copyId = await AzBlobContainers.Current.CloudCopyAsync(id, _cancelTokenSource.Token).ConfigureAwait(false);
                if (!string.IsNullOrEmpty(copyId))
                {
                    // Somehow we still got a (409) Conflict: PendingCopyOperation exception
                    // We'll call this Pending and it will get checked again on the next pass
                    if (copyId == "PendingCopyOperation")
                    {
                        action = BlobAction.Pending;
                    }
                    inErrorState = false;
                    if (errorRetry)
                    {
                        lock (_blobsInErrorStateLockObj)
                        {
                            _blobsInErrorStateDict.Remove(id);
                        }
                    }
                }
            }
            catch (TaskCanceledException tce)
            {
                if (!_cancelTokenSource.IsCancellationRequested)
                {
                    LogError("TaskCanceledException caught in method SynchronizeBlob", tce);
                }
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method SynchronizeBlob Id=" + id.ToString(), ex);
            }
            finally
            {
                if (inErrorState)
                {
                    action = BlobAction.Error;
                    if (!errorRetry)
                    {
                        PassDetails.ActionCounts.IncrementActionCount(action);
                    }
                    lock (_blobsInErrorStateLockObj)
                    {
                        _blobsInErrorStateDict[id] = DateTime.UtcNow.AddMinutes(_errorRetryWaitMinutes);
                        if (_blobsInErrorStateDict.Count >= _errorThreshold && !_cancelTokenSource.IsCancellationRequested)
                        {
                            ErrorThresholdReached();
                        }
                    }
                }
                else
                {
                    if (errorRetry)
                    {
                        PassDetails.ActionCounts.IncrementActionCount(BlobAction.ErrorCorrected);
                    }
                    else
                    {
                        PassDetails.ActionCounts.IncrementActionCount(action);
                    }
                }
                if ((_logNoOPs || action != BlobAction.NoOP) && !_cancelTokenSource.IsCancellationRequested)
                {
                    string message = String.Format("ID={0}; Action={1}; copyId={2}", id, GetActionString(action), copyId);
                    Log(message);
                }
            }
            return action;
        }

        private async Task RetryBlobsInErrorState()
        {
            bool cancelled = false;
            
            // Wait for the BlobsInErrorState to be loaded
            while (Interlocked.Read(ref _blobsInErrorStateLoaded) == 0)
            {
                Thread.Sleep(10000);
            }

            while (true)
            {
                try
                {
                    // Process the retries in blocks of 100
                    int idsToRetryCount;
                    IEnumerable<string> idsToRetry = null;
                    lock (_blobsInErrorStateLockObj)
                    {
                        idsToRetry =
                            (from kv in _blobsInErrorStateDict
                             where DateTime.UtcNow > kv.Value
                             orderby kv.Value
                             select kv.Key).Take(100);
                        idsToRetryCount = idsToRetry.Count();
                    }
                    
                    if (idsToRetryCount == 0) continue;

                    Task<BlobAction>[] retryTasks = new Task<BlobAction>[idsToRetryCount];
                    int index = 0;
                    foreach (string id in idsToRetry)
                    {
                        try
                        {
                            int currIndex = index;
                            // Need to get the Source blob so we have the ContentMD5 hash. Only a blob that came from the server will have the MD5 
                            CloudBlob blob = await AzBlobContainers.Current.GetCloudBlockBlobAsync(ContainerType.SourceBlob, id, _cancelTokenSource.Token).ConfigureAwait(false);
                            if (blob != null && blob.IsFromServer)
                            {
                                retryTasks[currIndex] = Task.Run(async () => await SynchronizeBlob(id, blob.Blob.Properties.ContentMD5, true).ConfigureAwait(false));
                            }
                            index++;
                        }
                        catch (TaskCanceledException tce)
                        {
                            if (!_cancelTokenSource.IsCancellationRequested)
                            {
                                LogError("TaskCanceledException caught in method RetryBlobsInErrorState", tce);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogError("Exception caught in method RetryBlobsInErrorState", ex);
                        }
                        finally
                        {
                            if (_cancelTokenSource.IsCancellationRequested)
                            {
                                cancelled = true;
                            }
                        }
                        if (cancelled) break;
                    }

                }
                catch (Exception ex)
                {
                    LogError("Exception caught in method RetryBlobsInErrorState", ex);
                }
                finally
                {
                    if (_cancelTokenSource.IsCancellationRequested)
                    {
                        cancelled = true;
                    }
                }
                if (cancelled) break;
                // Sleep a wee bit
                Thread.Sleep(1000);
            }
            Log("Exiting method RetryBlobsInErrorState");

        }

        /// <summary>
        /// Attempts to load the _destContentMD5s dictionary and BlobContinuationToken.
        /// First from the local ProgramDataDir, then the destination SyncLogContainer
        /// </summary>
        private void TryLoadState()
        {
            try
            {
                Task[] loadTasks = new Task[3];
                loadTasks[0] = Task.Run(() => LoadContentMD5s(_cancelTokenSource.Token));
                loadTasks[1] = Task.Run(() => LoadContinuationToken(), _cancelTokenSource.Token);
                loadTasks[2] = Task.Run(() => LoadBlobsInErrorState(_cancelTokenSource.Token));
                Task.WaitAll(loadTasks);
            }
            catch (Exception ex)
            {
                LogError("Exception while trying to load state:", ex);
            }
        }

        private void SaveState(DateTime logOrEndDateUtc)
        {
            if (DateTime.UtcNow >= _nextStateLogTime)
            {
                Task[] saveTasks = new Task[5];
                saveTasks[0] = Task.Run(() => SaveContentMD5s(), _cancelTokenSource.Token);
                saveTasks[1] = Task.Run(() => SaveContinuationToken(true), _cancelTokenSource.Token);
                saveTasks[2] = Task.Run(() => SaveBlobsInErrorState(), _cancelTokenSource.Token);
                saveTasks[3] = Task.Run(() => LogPassStats(logOrEndDateUtc));
                saveTasks[4] = Task.Run(() => SetMetadata());
                Task.WaitAll(saveTasks);
                _nextStateLogTime = DateTime.UtcNow.AddMinutes(_stateLogIntervalMinutes);
            }
            else
            {
                SaveContinuationToken(false);
            }
        }

        private void ProcessEndOfPass(DateTime endOfPassUtc)
        {
            // Allow Exceptions to bubble out to the Synchronize method
            PassDetails.State = PassState.Completed;
            if (PassDetails.Type == PassType.Full)
            {
                PassDetails.LastFullPassCompletedUtc = endOfPassUtc;
                PassDetails.SourceContainerCount = _contentMD5sDict.Count;
            }

            // set _nextStateLogTime in the past to save state and upload everything
            _nextStateLogTime = DateTime.MinValue;
            SaveState(endOfPassUtc);
            Log(String.Format("Finished {0} Pass {1}", PassDetails.Type.ToString(), PassDetails.Name));
            
            // Reset the Log files and PassDetails
            PassDetails.Reset();
            PassDetails.IncrementSequenceNumber();
            SetLogFileNames();
        }

        private async Task SetMetadata()
        {
            string dateFmt = "MM/dd/yy H:mm:ss";
            try
            {
                SortedDictionary<string, string> metaData = new SortedDictionary<string, string>();
                metaData["SyncedContainer"] = AzBlobContainers.Current.GetContainerUri(ContainerType.DestBlob);
                if (PassDetails.SyncedAsOfUtc.HasValue)
                {
                    metaData["SyncedAsOfUtc"] = PassDetails.SyncedAsOfUtc.Value.ToString(dateFmt);
                }
                if (PassDetails.Type == PassType.Full && PassDetails.LastFullPassCompletedUtc.HasValue)
                {
                    metaData["Count"] = String.Format("{0:#,##0}", PassDetails.SourceContainerCount);
                    metaData["CountAsOfUtc"] = PassDetails.LastFullPassCompletedUtc.Value.ToString(dateFmt);
                }
                await AzBlobContainers.Current.SetMetadataAsync(ContainerType.SourceBlob, metaData, _cancelTokenSource.Token).ConfigureAwait(false);
                metaData.Remove("SyncedContainer");
                if (metaData.Count > 0)
                {
                    await AzBlobContainers.Current.SetMetadataAsync(ContainerType.DestBlob, metaData, _cancelTokenSource.Token).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method SetMetadata()", ex);
            }
        }

        private void SetLogFileNames()
        {

            DateTime dt = DateTime.UtcNow;
            DateTime today = new DateTime(dt.Year, dt.Month, dt.Day);
            if (PassDetails.SequenceNumber < 0)
            {
                // Service has just started, figure out what sequence number we're on by checking the existing log files
                _logFileDate = today;
                int seqNo = 1;
                while (true)
                {
                    string tmpPassName = GeneratePassName(today, seqNo);
                    string logFileName = String.Format(_logFileFormat, tmpPassName);
                    if (File.Exists(logFileName))
                    {
                        seqNo++;
                    }
                    else
                    {
                        // Use the last file found
                        PassDetails.SequenceNumber = Math.Max(seqNo - 1, 1);
                        break;
                    }
                }
            }
            if (today > _logFileDate)
            {
                PassDetails.SequenceNumber = 1;
                PassDetails.ResetDailyStats();
                _logFileDate = today;
            }
            PassDetails.Name = GeneratePassName(today, PassDetails.SequenceNumber);
            _logFileName = String.Format(_logFileFormat, PassDetails.Name);
            _errorLogFileName = String.Format(_errorLogFileFormat, PassDetails.Name);
            _passDetailsLogFileName = String.Format(_passDetailsLogFileFormat, PassDetails.Name);
            AzBlobContainers.Current.StorageExceptionLogFile = String.Format(_storageExceptionFileFormat, PassDetails.Name);
            Task.Run(() => UploadLogFilesAsync(PassDetails.Name));
        }

        private static string GeneratePassName(DateTime dt, long seqNo)
        {
            string passSeq = String.Format("{0}_{1}_{2}.{3}",
                                    dt.Year,
                                    dt.Month.ToString("D2"),
                                    dt.Day.ToString("D2"),
                                    seqNo.ToString("D3"));
            return passSeq;
        }

        /// <summary>
        /// Uploads log files to the Destination SyncLog container Async
        /// </summary>
        /// <param name="currentPassName">
        /// The current PassName, we won't upload any of the current working logs
        /// </param>
        private async Task UploadLogFilesAsync(string currentPassName)
        {
            // Don't allow this method to be entered twice
            if (Interlocked.CompareExchange(ref _uploadLogFilesCalled, 1, 0) == 0)
            {
                try
                {
                    // If  is null, we haven't listed the files in the synclog container yet
                    if (_uploadedLogFiles == null)
                    {
                        await SetUploadedLogFiles();
                    }
                    // Get a list of the log files currently in the LogDirectory
                    List<string> logFiles = Directory.EnumerateFiles(_logDir, "AzSync*.Log").ToList();
                    foreach (string logFile in logFiles)
                    {
                        string fileName = Path.GetFileName(logFile);
                        if (!_uploadedLogFiles.ContainsKey(fileName) && !fileName.Contains(currentPassName))
                        {
                            _uploadedLogFiles[fileName] = false;
                        }
                    }

                    List<string> filesToUpload =
                        (from uf in _uploadedLogFiles
                         where !uf.Value
                         select uf.Key).ToList();
                    Task<bool>[] uploadTasks = new Task<bool>[filesToUpload.Count];
                    int currIndex = 0;
                    foreach (string fileToUpload in filesToUpload)
                    {
                        uploadTasks[currIndex] = Task.Run(async () => await 
                            AzBlobContainers.Current.TryUploadFileAsync(
                                ContainerType.DestLog,
                                Path.Combine(_logDir, fileToUpload),
                                fileToUpload, 
                                _cancelTokenSource.Token).ConfigureAwait(false));
                        currIndex++;
                    }
                    Task.WaitAll(uploadTasks);
                    string destSyncLogUri = AzBlobContainers.Current.GetContainerUri(ContainerType.DestLog);
                    for (int i = 0; i < uploadTasks.Length; i++)
                    {
                        if (uploadTasks[i].Result)
                        {
                            Log(String.Format("Uploaded log file {0} to {1}", Path.Combine(_logDir, filesToUpload[i]), destSyncLogUri));
                            _uploadedLogFiles[filesToUpload[i]] = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception caught in method UploadLogFiles():", ex);
                }
                finally
                {
                    // Release lock
                    Interlocked.Exchange(ref _uploadLogFilesCalled, 0);
                }
            }
            else
            {
                Log("Locked out of method UploadLogFiles()");
            }
        }

        private async Task SetUploadedLogFiles()
        {
            _uploadedLogFiles = new ConcurrentDictionary<string, bool>();
            try
            {
                BlobResultSegment resultSegment = await AzBlobContainers.Current.ListBlobsAsync(ContainerType.DestLog, null, _cancelTokenSource.Token).ConfigureAwait(false);
                while (resultSegment.ContinuationToken != null)
                {
                    foreach (var blob in resultSegment.Results)
                    {
                        _uploadedLogFiles[((CloudBlockBlob)blob).Name] = true;
                    }
                    resultSegment = await AzBlobContainers.Current.ListBlobsAsync(ContainerType.DestLog, null, _cancelTokenSource.Token).ConfigureAwait(false);
                }
                foreach (var blob in resultSegment.Results)
                {
                    _uploadedLogFiles[((CloudBlockBlob)blob).Name] = true;
                }
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method GetUploadedLogFiles():", ex);
            }
        }

        static long _errorThresholdReachCalled = 0;
        private void ErrorThresholdReached()
        {
            // Only call this method once
            if (Interlocked.CompareExchange(ref _errorThresholdReachCalled, 1, 0) == 0)
            {

                Log("Error Threshold Reached. Stopping Service ...");
                System.ServiceProcess.ServiceController controller =
                    new ServiceController(ConfigurationManager.AppSettings["ServiceName"]);
                controller.Stop();
            }
        }

        #region Logging

        static object _logLockoObj = new object();
        private void Log(string message)
        {
            // No exceptions from this method
            try
            {
                lock (_logLockoObj)
                {
                    using (StreamWriter sw = new StreamWriter(_logFileName, true))
                    {
                        sw.WriteLine("{0}: {1}", DateTime.UtcNow, message);
                    }
                }
            }
            catch { }
        }

        
        static object _errorLockoObj = new object();
        private void LogError(string message, Exception ex)
        {
            // No exceptions from this method
            try
            {
                lock (_errorLockoObj)
                {
                    using (StreamWriter sw = new StreamWriter(_errorLogFileName, true))
                    {
                        sw.WriteLine("{0}: {1}{2}{3}", DateTime.UtcNow, message, Environment.NewLine, ex);
                    }
                }
            }
            catch { }
        }

        private void LogPassStats(DateTime logOrEndDateUtc)
        {
            PassDetails.LogOrEndTimeUtc = logOrEndDateUtc;
            lock (_blobsInErrorStateLockObj)
            {
                PassDetails.StillInErrorState = _blobsInErrorStateDict.Count();
            }

            File.WriteAllText(_passDetailsLogFileName, PassDetails.ToString(Formatting.Indented));
        }

        private string GetActionString(BlobAction action)
        {
            return String.Format("***{0}***", action);
        }

        #endregion

        #endregion
    }
}
