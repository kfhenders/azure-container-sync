﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Kfh.AzSync
{
    public partial class Synchronizer
    {

        private async Task LoadContentMD5s(CancellationToken cancellationToken)
        {
            try
            {
                bool exists = File.Exists(_contentMD5FilePath);
                if (!exists && await AzBlobContainers.Current.TryDownloadFileAsync(ContainerType.DestLog, _contentMD5ZipFileName, _contentMD5ZipFilePath, _cancelTokenSource.Token).ConfigureAwait(false))
                {
                    string message = String.Format("Found file {0} in destination SyncLogContainer. Decompressing to {1}", _contentMD5ZipFileName, _contentMD5FilePath);
                    Log(message);
                    await GZip.DecompressFileAsync(_contentMD5ZipFilePath, _contentMD5FilePath).ConfigureAwait(false);
                    exists = true;
                }

                if (exists)
                {
                    Log("Loading ContentMD5s from " + _contentMD5FilePath);
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
                    using (StreamReader sr = new StreamReader(_contentMD5FilePath))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] kv = line.Split(new char[] { ',' });
                            _contentMD5sDict[kv[0]] = kv[1];
                            if (cancellationToken.IsCancellationRequested)
                            {
                                break;
                            }
                        }
                    }
                    watch.Stop();
                    Log(String.Format("Loaded {0} ContentMD5 records in {1}", String.Format("{0:#,##0}", _contentMD5sDict.Count), watch.Elapsed.ToString(@"hh\:mm\:ss")));
                }
            }
            catch (TaskCanceledException tce)
            {
                if (!_cancelTokenSource.IsCancellationRequested)
                {
                    LogError("TaskCanceledException caught in method LoadContentMD5s", tce);
                }
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method LoadContentMD5s", ex);
            }
        }

        private void SaveContentMD5s()
        {
            try
            {
                if (File.Exists(_contentMD5FilePath))
                {
                    File.Delete(_contentMD5FilePath);
                }
                var contents =
                    from b in _contentMD5sDict
                    where b.Value != null
                    select b.Key + "," + b.Value;
                File.AppendAllLines(_contentMD5FilePath, contents);
                Log(String.Format("Saved {0} ContentMD5s to {1}", String.Format("{0:#,##0}", _contentMD5sDict.Count), _contentMD5FilePath));
                Task.Run(() => CompressAndUploadContentMD5sAsync());
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method SaveContentMD5s", ex);
            }
        }

        static int _contentMD5FileInUse = 0;
        /// <summary>
        /// Compresses the ContentMD5s file and uploads the compressed file to the destination SyncLogContainer
        /// </summary>
        /// <returns></returns>
        private async Task CompressAndUploadContentMD5sAsync()
        {
            // Don't allow this method to be entered twice
            if (Interlocked.CompareExchange(ref _contentMD5FileInUse, 1, 0) == 0)
            {
                try
                {
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
                    Log(String.Format("Compressing file {0} to {1}", _contentMD5FilePath, _contentMD5ZipFilePath));
                    await GZip.CompressFileAsync(_contentMD5FilePath, _contentMD5ZipFilePath).ConfigureAwait(false);
                    bool uploaded = await AzBlobContainers.Current.TryUploadFileAsync(ContainerType.DestLog, _contentMD5ZipFilePath, _contentMD5ZipFileName, _cancelTokenSource.Token).ConfigureAwait(false);
                    watch.Stop();
                    string message = String.Format("Upload of file {0} to destination SyncLogContainer {1} in {2}",
                        _contentMD5ZipFilePath,  //0
                        uploaded ? "Succeeded" : "Failed", //1
                        watch.Elapsed.ToString(@"hh\:mm\:ss") //2
                        );
                    Log(message);
                }
                catch (TaskCanceledException tce)
                {
                    if (!_cancelTokenSource.IsCancellationRequested)
                    {
                        LogError("TaskCanceledException caught in method CompressAndUploadContentMD5sAsync", tce);
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception caught in method CompressAndUploadContentMD5sAsync", ex);
                }
                finally
                {
                    // Release it
                    Interlocked.Exchange(ref _contentMD5FileInUse, 0);
                }
            }
            else
            {
                Log("Locked out of method CompressAndUploadContentMD5sAsync");
            }
        }
    }
}
