﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Kfh.AzSync
{
    public partial class Synchronizer
    {
        private async Task LoadContinuationToken()
        {
            try
            {
                bool exists = File.Exists(_continuationTokenFilePath);
                if (!exists && await AzBlobContainers.Current.TryDownloadFileAsync(ContainerType.DestLog, _continuationTokenFileName, _continuationTokenFilePath, _cancelTokenSource.Token).ConfigureAwait(false))
                {
                    string message = String.Format("Found file {0} in destination SyncLogContainer. Downloaded to {1}", _continuationTokenFileName, _continuationTokenFilePath);
                    Log(message);
                    exists = true;
                }
                if (exists)
                {
                    _continuationToken = await JsonConvert.DeserializeObjectAsync<BlobContinuationToken>(File.ReadAllText(_continuationTokenFilePath)).ConfigureAwait(false);
                    Log(String.Format("Loaded continuation token from {0}", _continuationTokenFilePath));
                }
            }
            catch (TaskCanceledException tce)
            {
                if (!_cancelTokenSource.IsCancellationRequested)
                {
                    LogError("TaskCanceledException caught in method LoadContinuationToken", tce);
                }
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method LoadContinuationToken", ex);
            }
        }

        private void SaveContinuationToken(bool uploadToo)
        {
            try
            {
                if (_continuationToken != null)
                {
                    string bctJson = JsonConvert.SerializeObject(_continuationToken);
                    File.WriteAllText(_continuationTokenFilePath, bctJson);
                    if (uploadToo)
                    {
                        Task.Run(() => UploadContinuationTokenAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method SaveContinuationToken", ex);
            }
        }

        static int _continuationTokenFileInUse = 0;
        private async Task UploadContinuationTokenAsync()
        {
            // Don't allow this method to be entered twice
            if (Interlocked.CompareExchange(ref _continuationTokenFileInUse, 1, 0) == 0)
            {
                try
                {
                    bool uploaded = await AzBlobContainers.Current.TryUploadFileAsync(ContainerType.DestLog, _continuationTokenFilePath, _continuationTokenFileName, _cancelTokenSource.Token).ConfigureAwait(false);
                    string message = String.Format("Upload of file {0} to destination SyncLogContainer {1}", _continuationTokenFilePath, uploaded ? "Succeeded" : "Failed");
                    Log(message);
                }
                catch (TaskCanceledException tce)
                {
                    if (!_cancelTokenSource.IsCancellationRequested)
                    {
                        LogError("TaskCanceledException caught in method UploadContinuationToken", tce);
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception caught in method UploadContinuationToken", ex);
                }
                finally
                {
                    // Release it
                    Interlocked.Exchange(ref _continuationTokenFileInUse, 0);
                }
            }
            else
            {
                Log("Locked out of method UploadContinuationTokenAsync");
            }
        }
    }
}
