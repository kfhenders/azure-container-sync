﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Kfh.AzSync
{
    public partial class Synchronizer
    {

        private async Task LoadBlobsInErrorState(CancellationToken cancellationToken)
        {
            try
            {
                bool exists = File.Exists(_blobsInErrorStateFilePath);
                if (!exists && await AzBlobContainers.Current.TryDownloadFileAsync(ContainerType.DestLog, _blobsInErrorStateZipFileName, _blobsInErrorStateZipFilePath, _cancelTokenSource.Token).ConfigureAwait(false))
                {
                    string message = String.Format("Found file {0} in destination SyncLogContainer. Decompressing to {1}", _blobsInErrorStateZipFileName, _blobsInErrorStateFilePath);
                    Log(message);
                    await GZip.DecompressFileAsync(_blobsInErrorStateZipFilePath, _blobsInErrorStateFilePath).ConfigureAwait(false);
                    exists = true;
                }

                if (exists)
                {
                    Log("Loading BlobsInErrorState from " + _blobsInErrorStateFilePath);
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
                    lock (_blobsInErrorStateLockObj)
                    {
                        using (StreamReader sr = new StreamReader(_blobsInErrorStateFilePath))
                        {
                            string id;
                            while ((id = sr.ReadLine()) != null)
                            {
                                _blobsInErrorStateDict[id] = DateTime.UtcNow.AddMinutes(_errorRetryWaitMinutes);
                                if (cancellationToken.IsCancellationRequested)
                                {
                                    break;
                                }
                            }
                        }
                        watch.Stop();
                        Log(String.Format("Loaded {0} BlobsInErrorState records in {1}", String.Format("{0:#,##0}", _blobsInErrorStateDict.Count), watch.Elapsed.ToString(@"hh\:mm\:ss")));
                    }
                }
            }
            catch (TaskCanceledException tce)
            {
                if (!_cancelTokenSource.IsCancellationRequested)
                {
                    LogError("TaskCanceledException caught in method LoadBlobsInErrorState", tce);
                }
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method LoadBlobsInErrorState", ex);
            }
            finally
            {
                Interlocked.Exchange(ref _blobsInErrorStateLoaded, 1);
            }
        }

        private void SaveBlobsInErrorState()
        {
            try
            {
                if (File.Exists(_blobsInErrorStateFilePath))
                {
                    File.Delete(_blobsInErrorStateFilePath);
                }
                lock (_blobsInErrorStateLockObj)
                {
                    File.AppendAllLines(_blobsInErrorStateFilePath, _blobsInErrorStateDict.Keys);
                    Log(String.Format("Saved {0} BlobsInErrorState to {1}", String.Format("{0:#,##0}", _blobsInErrorStateDict.Count), _blobsInErrorStateFilePath));
                }
                Task.Run(() => CompressAndUploadBlobsInErrorState());
            }
            catch (Exception ex)
            {
                LogError("Exception caught in method BlobsInErrorState", ex);
            }
        }

        static int _blobsInErrorStateFileInUse = 0;
        /// <summary>
        /// Compresses the BlobsInErrorState file and uploads the compressed file to the destination SyncLogContainer
        /// </summary>
        /// <returns></returns>
        private async Task CompressAndUploadBlobsInErrorState()
        {
            // Don't allow this method to be entered twice
            if (Interlocked.CompareExchange(ref _blobsInErrorStateFileInUse, 1, 0) == 0)
            {
                try
                {
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
                    Log(String.Format("Compressing file {0} to {1}", _blobsInErrorStateFilePath, _blobsInErrorStateZipFilePath));
                    await GZip.CompressFileAsync(_blobsInErrorStateFilePath, _blobsInErrorStateZipFilePath).ConfigureAwait(false);
                    bool uploaded = await AzBlobContainers.Current.TryUploadFileAsync(ContainerType.DestLog, _blobsInErrorStateZipFilePath, _blobsInErrorStateZipFileName, _cancelTokenSource.Token).ConfigureAwait(false);
                    watch.Stop();
                    string message = String.Format("Upload of file {0} to destination SyncLogContainer {1} in {2}",
                        _blobsInErrorStateZipFilePath,  //0
                        uploaded ? "Succeeded" : "Failed", //1
                        watch.Elapsed.ToString(@"hh\:mm\:ss") //2
                        );
                    Log(message);
                }
                catch (TaskCanceledException tce)
                {
                    if (!_cancelTokenSource.IsCancellationRequested)
                    {
                        LogError("TaskCanceledException caught in method CompressAndUploadBlobsInErrorState", tce);
                    }
                }
                catch (Exception ex)
                {
                    LogError("Exception caught in method CompressAndUploadBlobsInErrorState", ex);
                }
                finally
                {
                    // Release it
                    Interlocked.Exchange(ref _blobsInErrorStateFileInUse, 0);
                }
            }
            else
            {
                Log("Locked out of method CompressAndUploadBlobsInErrorState");
            }
        }
    }
}
