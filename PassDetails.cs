﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace Kfh.AzSync
{
    /// <summary>
    /// Thread safe class for maintaining Pass statistics
    /// </summary>
    public static class PassDetails
    {
        public static void Reset()
        {
            Name = null;
            Type = PassType.Unknown;
            State = PassState.Unknown;
            lock (_startTimeUtcLockObj) { _startTimeUtc = null; };
            lock (_logOrEndTimeUtcLockObj) { _logOrEndTimeUtc = null; };
            Interlocked.Exchange(ref _processedCount, 0);
            Interlocked.Exchange(ref _errorRetryCount, 0);
            Interlocked.Exchange(ref _stillInErrorState, 0);
            ActionCounts.Reset();
        }

        public static void ResetDailyStats()
        {
            DailyStats.Reset();
        }

        static long _seqNo = -1;
        /// <summary>
        /// NOTE: Not updated on Reset()
        /// </summary>
        public static long SequenceNumber
        {
            get { return Interlocked.Read(ref _seqNo); }
            set { Interlocked.Exchange(ref _seqNo, value); }
        }
        public static long IncrementSequenceNumber()
        {
            return Interlocked.Increment(ref _seqNo);
        }

        static string _name = null;
        static object _nameLockObj = new object();
        public static string Name
        {
            get { lock (_nameLockObj) { return _name; } }
            set { lock (_nameLockObj) { _name = value; } }
        }

        static PassType _type = PassType.Unknown;
        static object _typeLockObj = new object();
        public static PassType Type
        {
            get { lock (_typeLockObj) { return _type; } }
            set { lock (_typeLockObj) _type = value; }
        }

        static PassState _state = PassState.Unknown;
        static Object _stateLockObj = new object();
        public static PassState State
        {
            get { lock (_stateLockObj) { return _state; } }
            set { lock (_stateLockObj) { _state = value; } }
        }

        static DateTime? _startTimeUtc = null;
        static Object _startTimeUtcLockObj = new object();
        public static DateTime StartTimeUtc
        {
            get { lock (_startTimeUtcLockObj) { return _startTimeUtc.HasValue ? _startTimeUtc.Value : DateTime.MinValue; } }
            set
            {
                lock (_startTimeUtcLockObj)
                {
                    _startTimeUtc = value;
                    if (SequenceNumber == 1 || DailyStats.StartTimeUtc == DateTime.MinValue)
                    {
                        DailyStats.StartTimeUtc = value;
                    }
                }
            }
        }

        static DateTime? _logOrEndTimeUtc = null;
        static Object _logOrEndTimeUtcLockObj = new object();
        public static DateTime LogOrEndTimeUtc
        {
            get { lock (_logOrEndTimeUtcLockObj) { return _logOrEndTimeUtc.HasValue ? _logOrEndTimeUtc.Value : DateTime.MinValue; } }
            set { lock (_logOrEndTimeUtcLockObj) { _logOrEndTimeUtc = value; } }
        }

        static DateTime? _lastFullPassCompletedUtc = null;
        static Object _lastFullPassCompletedUtcLockObj = new object();
        /// <summary>
        /// NOTE: Not updated on Reset()
        /// </summary>
        public static DateTime? LastFullPassCompletedUtc
        {
            get { lock (_lastFullPassCompletedUtcLockObj) { return _lastFullPassCompletedUtc; } }
            set
            {
                lock (_lastFullPassCompletedUtcLockObj)
                {
                    // If this is at least the second completed full pass than SyncedAsOfUtc is when the previous FullPass completed
                    if (_lastFullPassCompletedUtc.HasValue)
                    {
                        _syncedAsOfUtc = _lastFullPassCompletedUtc;
                    }
                    _lastFullPassCompletedUtc = value;
                }
            }
        }

        static DateTime? _syncedAsOfUtc = null;
        public static DateTime? SyncedAsOfUtc
        {
            get { return _syncedAsOfUtc; }
        }

        public static TimeSpan Elapsed
        {
            get
            {
                return LogOrEndTimeUtc - StartTimeUtc;
            }
        }

        static long _sourceContainerCount;
        /// <summary>
        /// NOTE: Not updated on Reset()
        /// </summary>
        public static long SourceContainerCount
        {
            get { return Interlocked.Read(ref _sourceContainerCount); }
            set { Interlocked.Exchange(ref _sourceContainerCount, value); }
        }

        static long _processedCount;
        public static long ProcessedCount { get { return Interlocked.Read(ref _processedCount); } }
        public static long IncrementProcessedCount()
        {
            DailyStats.IncrementProcessedCount();
            return Interlocked.Increment(ref _processedCount);
        }

        static long _errorRetryCount;
        public static long ErrorRetryCount { get { return Interlocked.Read(ref _errorRetryCount); } }
        public static long IncrementErrorRetryCount()
        {
            DailyStats.IncrementErrorRetryCount();
            return Interlocked.Increment(ref _errorRetryCount);
        }

        static long _stillInErrorState;
        public static long StillInErrorState
        {
            get { return Interlocked.Read(ref _stillInErrorState); }
            set { Interlocked.Exchange(ref _stillInErrorState, value); }
        }

        public static string ToString(Formatting jsonFormatting)
        {
            string dateFmt = "MM/dd/yy H:mm:ss";

            var stats = new JObject() as dynamic;
            if (_syncedAsOfUtc.HasValue)
            {
                stats.SyncedAsOfUtc = _syncedAsOfUtc.Value.ToString(dateFmt);
            }
            var passStats = new JObject() as dynamic;
            passStats.Name = Name;
            passStats.Type = Type.ToString();
            passStats.State = State.ToString();
            passStats.StartTime = _startTimeUtc.HasValue ? StartTimeUtc.ToString(dateFmt) : "Unknown";
            if (State == PassState.Completed)
            {
                passStats.EndTime = _logOrEndTimeUtc.HasValue ? LogOrEndTimeUtc.ToString(dateFmt) : "Unknown";
            }
            else
            {
                passStats.LogTime = _logOrEndTimeUtc.HasValue ? LogOrEndTimeUtc.ToString(dateFmt) : "Unknown";
            }
            if (Elapsed != TimeSpan.Zero)
            {
                passStats.Elapsed = Elapsed.ToString(@"hh\:mm\:ss");
            }
            if (_lastFullPassCompletedUtc.HasValue)
            {
                passStats.LastFullPassCompletedUtc = _lastFullPassCompletedUtc.Value.ToString(dateFmt);
            }
            if (_syncedAsOfUtc.HasValue)
            {
                passStats.SyncedAsOfUtc = _syncedAsOfUtc.Value.ToString(dateFmt);
            }

            if (Type == PassType.Full && State == PassState.Completed)
            {
                passStats.SourceContainerCount = String.Format("{0:#,##0}", SourceContainerCount);
            }

            passStats.ProcessedCount = String.Format("{0:#,##0}", ProcessedCount);
            passStats.ErrorRetryCount = String.Format("{0:#,##0}", ErrorRetryCount);
            passStats.StillInErrorState = String.Format("{0:#,##0}", StillInErrorState);

            if (Elapsed != TimeSpan.Zero)
            {
                var rates = new JObject() as dynamic;
                rates.PerSecond = String.Format("{0:#,##0}", ProcessedCount / Elapsed.TotalSeconds);
                rates.PerMinute = String.Format("{0:#,##0}", ProcessedCount / Elapsed.TotalMinutes);
                rates.PerHour = String.Format("{0:#,##0}", ProcessedCount / Elapsed.TotalHours);
                passStats.Rates = rates;
            }

            var actionCounts = new JObject() as dynamic;
            actionCounts.NoOp = String.Format("{0:#,##0}", ActionCounts.NoOp);
            actionCounts.Add = String.Format("{0:#,##0}", ActionCounts.Add);
            actionCounts.Update = String.Format("{0:#,##0}", ActionCounts.Update);
            actionCounts.Repair = String.Format("{0:#,##0}", ActionCounts.Repair);
            actionCounts.Pending = String.Format("{0:#,##0}", ActionCounts.Pending);
            actionCounts.Error = String.Format("{0:#,##0}", ActionCounts.Error);
            actionCounts.ErrorsCorrected = String.Format("{0:#,##0}", ActionCounts.ErrorCorrected);
            passStats.ActionCounts = actionCounts;

            stats.Pass = passStats;
            stats.Day = DailyStats.JsonStats;

            return stats.ToString(jsonFormatting);

        }

        public static class ActionCounts
        {

            private static long _noOp = 0;
            public static long NoOp
            {
                get { return Interlocked.Read(ref _noOp); }
            }

            private static long _add = 0;
            public static long Add
            {
                get { return Interlocked.Read(ref _add); }
            }

            private static long _repair = 0;
            public static long Repair
            {
                get { return Interlocked.Read(ref _repair); }
            }

            private static long _update = 0;
            public static long Update
            {
                get { return Interlocked.Read(ref _update); }
            }

            private static long _pending = 0;
            public static long Pending
            {
                get { return Interlocked.Read(ref _pending); }
            }
            private static long _error = 0;
            public static long Error
            {
                get { return Interlocked.Read(ref _error); }
            }

            private static long _errorCorrected = 0;
            public static long ErrorCorrected
            {
                get { return Interlocked.Read(ref _errorCorrected); }
            }

            public static void IncrementActionCount(BlobAction action)
            {
                switch (action)
                {
                    case BlobAction.NoOP:
                        Interlocked.Increment(ref _noOp);
                        break;
                    case BlobAction.Add:
                        Interlocked.Increment(ref _add);
                        break;
                    case BlobAction.Update:
                        Interlocked.Increment(ref _update);
                        break;
                    case BlobAction.Repair:
                        Interlocked.Increment(ref _repair);
                        break;
                    case BlobAction.Pending:
                        Interlocked.Increment(ref _pending);
                        break;
                    case BlobAction.Error:
                        Interlocked.Increment(ref _error);
                        break;
                    case BlobAction.ErrorCorrected:
                        Interlocked.Increment(ref _errorCorrected);
                        break;
                    default:
                        // N/A
                        break;
                }
                DailyStats.ActionCounts.IncrementActionCount(action);
            }

            public static void Reset()
            {
                Interlocked.Exchange(ref _noOp, 0);
                Interlocked.Exchange(ref _add, 0);
                Interlocked.Exchange(ref _update, 0);
                Interlocked.Exchange(ref _repair, 0);
                Interlocked.Exchange(ref _pending, 0);
                Interlocked.Exchange(ref _error, 0);
                Interlocked.Exchange(ref _errorCorrected, 0);

            }
        }

        private static class DailyStats
        {
            public static void Reset()
            {
                lock (_startTimeUtcLockObj) { _startTimeUtc = null; };
                Interlocked.Exchange(ref _processedCount, 0);
                Interlocked.Exchange(ref _errorRetryCount, 0);
                ActionCounts.Reset();
            }

            static DateTime? _startTimeUtc = null;
            static Object _startTimeUtcLockObj = new object();
            public static DateTime StartTimeUtc
            {
                get { lock (_startTimeUtcLockObj) { return _startTimeUtc.HasValue ? _startTimeUtc.Value : DateTime.MinValue; } }
                set { lock (_startTimeUtcLockObj) { _startTimeUtc = value; } }
            }

            public static TimeSpan Elapsed
            {
                get
                {
                    return LogOrEndTimeUtc - StartTimeUtc;
                }
            }

            static long _processedCount;
            public static long ProcessedCount { get { return Interlocked.Read(ref _processedCount); } }
            public static long IncrementProcessedCount()
            {
                return Interlocked.Increment(ref _processedCount);
            }

            static long _errorRetryCount;
            public static long ErrorRetryCount { get { return Interlocked.Read(ref _errorRetryCount); } }
            public static long IncrementErrorRetryCount()
            {
                return Interlocked.Increment(ref _errorRetryCount);
            }

            public static class ActionCounts
            {

                private static long _noOp = 0;
                public static long NoOp
                {
                    get { return Interlocked.Read(ref _noOp); }
                }

                private static long _add = 0;
                public static long Add
                {
                    get { return Interlocked.Read(ref _add); }
                }

                private static long _repair = 0;
                public static long Repair
                {
                    get { return Interlocked.Read(ref _repair); }
                }

                private static long _update = 0;
                public static long Update
                {
                    get { return Interlocked.Read(ref _update); }
                }

                private static long _pending = 0;
                public static long Pending
                {
                    get { return Interlocked.Read(ref _pending); }
                }
                private static long _error = 0;
                public static long Error
                {
                    get { return Interlocked.Read(ref _error); }
                }

                private static long _errorCorrected = 0;
                public static long ErrorCorrected
                {
                    get { return Interlocked.Read(ref _errorCorrected); }
                }

                public static void IncrementActionCount(BlobAction action)
                {
                    switch (action)
                    {
                        case BlobAction.NoOP:
                            Interlocked.Increment(ref _noOp);
                            break;
                        case BlobAction.Add:
                            Interlocked.Increment(ref _add);
                            break;
                        case BlobAction.Update:
                            Interlocked.Increment(ref _update);
                            break;
                        case BlobAction.Repair:
                            Interlocked.Increment(ref _repair);
                            break;
                        case BlobAction.Pending:
                            Interlocked.Increment(ref _pending);
                            break;
                        case BlobAction.Error:
                            Interlocked.Increment(ref _error);
                            break;
                        case BlobAction.ErrorCorrected:
                            Interlocked.Increment(ref _errorCorrected);
                            break;
                        default:
                            // N/A
                            break;
                    }
                }

                public static void Reset()
                {
                    Interlocked.Exchange(ref _noOp, 0);
                    Interlocked.Exchange(ref _add, 0);
                    Interlocked.Exchange(ref _update, 0);
                    Interlocked.Exchange(ref _repair, 0);
                    Interlocked.Exchange(ref _pending, 0);
                    Interlocked.Exchange(ref _error, 0);
                    Interlocked.Exchange(ref _errorCorrected, 0);

                }
            }

            public static JObject JsonStats
            {
                get
                {
                    string dateFmt = "MM/dd/yy H:mm:ss";

                    var dailyStats = new JObject() as dynamic;
                    dailyStats.StartTime = _startTimeUtc.HasValue ? StartTimeUtc.ToString(dateFmt) : "Unknown";
                    if (State == PassState.Completed)
                    {
                        dailyStats.EndTime = _logOrEndTimeUtc.HasValue ? LogOrEndTimeUtc.ToString(dateFmt) : "Unknown";
                    }
                    else
                    {
                        dailyStats.LogTime = _logOrEndTimeUtc.HasValue ? LogOrEndTimeUtc.ToString(dateFmt) : "Unknown";
                    }
                    if (Elapsed != TimeSpan.Zero)
                    {
                        dailyStats.Elapsed = Elapsed.ToString(@"d\:hh\:m\:ss");
                    }

                    dailyStats.ProcessedCount = String.Format("{0:#,##0}", ProcessedCount);
                    dailyStats.ErrorRetryCount = String.Format("{0:#,##0}", ErrorRetryCount);
                    dailyStats.StillInErrorState = String.Format("{0:#,##0}", StillInErrorState);

                    if (Elapsed != TimeSpan.Zero)
                    {
                        var rates = new JObject() as dynamic;
                        rates.PerSecond = String.Format("{0:#,##0}", ProcessedCount / Elapsed.TotalSeconds);
                        rates.PerMinute = String.Format("{0:#,##0}", ProcessedCount / Elapsed.TotalMinutes);
                        rates.PerHour = String.Format("{0:#,##0}", ProcessedCount / Elapsed.TotalHours);
                        dailyStats.Rates = rates;
                    }

                    var actionCounts = new JObject() as dynamic;
                    actionCounts.NoOp = String.Format("{0:#,##0}", ActionCounts.NoOp);
                    actionCounts.Add = String.Format("{0:#,##0}", ActionCounts.Add);
                    actionCounts.Update = String.Format("{0:#,##0}", ActionCounts.Update);
                    actionCounts.Repair = String.Format("{0:#,##0}", ActionCounts.Repair);
                    actionCounts.Pending = String.Format("{0:#,##0}", ActionCounts.Pending);
                    actionCounts.Error = String.Format("{0:#,##0}", ActionCounts.Error);
                    actionCounts.ErrorsCorrected = String.Format("{0:#,##0}", ActionCounts.ErrorCorrected);
                    dailyStats.ActionCounts = actionCounts;

                    return dailyStats;
                }
            }
        }
    }
}
