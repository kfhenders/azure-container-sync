﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Configuration;
using Topshelf;

namespace Kfh.AzSync
{
    class Program
    {
        static void Main(string[] args)
        {

            // Use Topshelf to instantiate the service
            HostFactory.Run(x =>
            {
                x.Service<Synchronizer>(s =>
                {
                    s.ConstructUsing(name => new Synchronizer());
                    s.WhenStarted(sync => sync.Start());
                    s.WhenStopped(sync => sync.Stop());
                    s.WhenShutdown(sync => sync.Shutdown());
                });
                x.RunAsLocalSystem();
                x.EnableShutdown();
                x.SetDescription(ConfigurationManager.AppSettings["Description"]);
                x.SetDisplayName(ConfigurationManager.AppSettings["DisplayName"]);
                x.SetServiceName(ConfigurationManager.AppSettings["ServiceName"]);
            });
        }
    }
}
