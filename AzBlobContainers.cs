﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using Kfh.AzSync.Config;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Kfh.AzSync
{

    public class AzBlobContainers
    {

        const string SASPolicyName = "Kfh.AzSync";

        // Let the SAS last 24 hours
        static object _lockObj = new object();
        static int _sourceSASExpireHours = 24;
        static DateTime _sourceSASExpires = DateTime.UtcNow.AddHours(-1);
        static string _sourceSAS = null;

        CloudStorageAccount _sourceAccount;
        CloudBlobClient _sourceClient;
        CloudBlobContainer _sourceBlobContainer;
        CloudStorageAccount _destAccount;
        CloudBlobClient _destClient;
        CloudBlobContainer _destBlobContainer;
        CloudBlobContainer _destLogContainer;


        #region Singleton Pattern

        private static AzBlobContainers _current = null;

        private AzBlobContainers()
        {
            _sourceAccount = CloudStorageAccount.Parse(BlobContainerSettings.Current.Source.ConnectionString);
            _sourceClient = _sourceAccount.CreateCloudBlobClient();
            _sourceBlobContainer = _sourceClient.GetContainerReference(BlobContainerSettings.Current.Source.BlobContainer);

            _destAccount = CloudStorageAccount.Parse(BlobContainerSettings.Current.Dest.ConnectionString);
            _destClient = _destAccount.CreateCloudBlobClient();
            _destBlobContainer = _destClient.GetContainerReference(BlobContainerSettings.Current.Dest.BlobContainer);
            _destBlobContainer.CreateIfNotExists();
            _destLogContainer = _destClient.GetContainerReference(BlobContainerSettings.Current.Dest.SyncLogContainer);
            _destLogContainer.CreateIfNotExists();

        }

        public static AzBlobContainers Current
        {
            get
            {
                if (_current == null)
                {
                    lock (_lockObj)
                    {
                        if (_current == null)
                        {
                            _current = new AzBlobContainers();
                        }
                    }
                }
                return _current;
            }
        }

        #endregion

        #region public Members

        public string StorageExceptionLogFile { get; set; }

        public async Task<BlobResultSegment> ListBlobsAsync(ContainerType containerType, BlobContinuationToken continuationToken, CancellationToken cancellationToken)
        {
            CloudBlobContainer container = GetContainer(containerType);
            return await container.ListBlobsSegmentedAsync(continuationToken, cancellationToken).ConfigureAwait(false);
        }

        public async Task<CloudBlob> GetCloudBlockBlobAsync(ContainerType containerType, string id, CancellationToken cancellationToken)
        {
            CloudBlob cloudBlob = null;
            CloudBlobContainer container = GetContainer(containerType);
            try
            {
                ICloudBlob serverBlob = await container.GetBlobReferenceFromServerAsync(id, cancellationToken).ConfigureAwait(false);
                cloudBlob = new CloudBlob
                {
                    Blob = serverBlob,
                    IsFromServer = true
                };
            }
            catch (StorageException ex)
            {
                // Don't bother logging 404s
                if (ex.RequestInformation.HttpStatusCode != 404)
                {
                    LogStorageException("StorageException caught in method GetCloudBlockBlob; Id=" + id, ex);
                }
                cloudBlob = new CloudBlob
                {
                    Blob = container.GetBlockBlobReference(id),
                    IsFromServer = false
                };
            }

            return cloudBlob;
        }

        public async Task<bool> AbortCopyAsync(ICloudBlob blob, string copyId, CancellationToken cancellationToken)
        {
            bool aborted = false;
            try
            {
                await blob.AbortCopyAsync(copyId, cancellationToken).ConfigureAwait(false);
                aborted = true;
            }
            catch (StorageException ex)
            {
                // Handle (409) Conflict: NoPendingCopyOperation.
                // Not sure how it gets here since it's supposed to be Pending if we got here
                if (ex.RequestInformation.HttpStatusCode!= 409)
                {
                    LogStorageException("StorageException caught in method AbortCopyAsync; Id=" + blob.Name, ex);
                    throw;
                }
            }
            return aborted;
        }
        /// <summary>
        /// Copies the blob from source to destination 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> CloudCopyAsync(string id, CancellationToken cancellationToken)
        {
            string result = null;
            try
            {
                BlobRequestOptions requestOptions = new BlobRequestOptions
                {
                    ServerTimeout = TimeSpan.FromSeconds(5)
                };
                var sourceBlob = _sourceBlobContainer.GetBlockBlobReference(id);
                var destBlob = _destBlobContainer.GetBlockBlobReference(id);
                Uri sourceSasUri = new Uri(sourceBlob.StorageUri.PrimaryUri + SourceSAS);
                result = await destBlob.StartCopyFromBlobAsync(sourceSasUri, null, null, requestOptions, null, cancellationToken).ConfigureAwait(false);
            }
            catch (StorageException ex)
            {
                // Handle (409) Conflict: PendingCopyOperation
                // Not sure how it gets here since we're supposed to abort it before we got here, but it happens
                if (ex.RequestInformation.HttpStatusCode == 409)
                {
                    result = "PendingCopyOperation";
                }
                else
                {
                    LogStorageException("StorageException caught in method CloudCopy; Id=" + id, ex);
                    throw;
                }
            }
            return result;

        }

        public async Task<bool> TryDownloadFileAsync(ContainerType containerType, string fileName, string destinationFile, CancellationToken cancellationToken)
        {
            CloudBlobContainer container = GetContainer(containerType);
            return await DownloadFile(container, fileName, destinationFile, cancellationToken).ConfigureAwait(false);
        }

        public async Task<bool> TryUploadFileAsync(ContainerType containerType, string sourceFile, string destinationFileName, CancellationToken cancellationToken)
        {
            CloudBlobContainer container = GetContainer(containerType);
            return await UploadFile(container, sourceFile, destinationFileName, cancellationToken).ConfigureAwait(false);
        }


        public async Task SetMetadataAsync(ContainerType containerType, IDictionary<string, string> metaData, CancellationToken cancellationToken)
        {
            if (metaData == null || metaData.Count == 0) return;
            CloudBlobContainer container = GetContainer(containerType);
            await container.FetchAttributesAsync(cancellationToken).ConfigureAwait(false);
            foreach (var kv in metaData)
            {
                container.Metadata[kv.Key] = kv.Value;
            }
            await container.SetMetadataAsync(cancellationToken).ConfigureAwait(false);
        }

        public string GetContainerUri(ContainerType containerType)
        {
            CloudBlobContainer container = GetContainer(containerType);
            return container.StorageUri.PrimaryUri.AbsoluteUri;
        }

        #endregion

        #region private members

        private async Task<bool> DownloadFile(CloudBlobContainer container, string fileName, string destinationFile, CancellationToken cancellationToken)
        {
            bool found = false;
            var blob = container.GetBlockBlobReference(fileName);
            try
            {
                await blob.DownloadToFileAsync(destinationFile, FileMode.Create, cancellationToken).ConfigureAwait(false);
                found = true;
            }
            catch (StorageException ex)
            {
                // Don't bother logging 404s
                if (ex.RequestInformation.HttpStatusCode != 404)
                {
                    LogStorageException("StorageException caught in method DownloadFile; fileName=" + fileName, ex);
                }
            }
            return found;
        }

        private async Task<bool> UploadFile(CloudBlobContainer container, string sourceFile, string destinationFileName, CancellationToken cancellationToken)
        {
            bool success = false;
            var blob = container.GetBlockBlobReference(destinationFileName);
            try
            {
                await blob.UploadFromFileAsync(sourceFile, FileMode.Open, cancellationToken).ConfigureAwait(false);
                success = true;
            }
            catch (StorageException ex)
            {
                LogStorageException("StorageException caught in method UploadFile; fileName=" + destinationFileName, ex);
            }
            return success;
        }

        #region SAS 
        private string SourceSAS
        {
            get
            {
                if (String.IsNullOrEmpty(_sourceSAS) || DateTime.UtcNow > _sourceSASExpires)
                {
                    lock (_lockObj)
                    {
                        if (String.IsNullOrEmpty(_sourceSAS) || DateTime.UtcNow > _sourceSASExpires)
                        {
                            SetSourceSAS();
                        }
                    }
                }
                return _sourceSAS;
            }
        }

        private void SetSourceSAS()
        {
            SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy();
            //policy.SharedAccessStartTime = DateTime.UtcNow.AddMinutes(10);
            policy.SharedAccessExpiryTime = DateTime.UtcNow.AddHours(_sourceSASExpireHours);
            policy.Permissions = SharedAccessBlobPermissions.Read;

            BlobContainerPermissions sourceContainerPermissions = new BlobContainerPermissions();
            sourceContainerPermissions.SharedAccessPolicies.Clear();
            sourceContainerPermissions.SharedAccessPolicies.Add(SASPolicyName, policy);
            _sourceBlobContainer.SetPermissions(sourceContainerPermissions);
            _sourceSAS = _sourceBlobContainer.GetSharedAccessSignature(new SharedAccessBlobPolicy(), SASPolicyName);
            _sourceSASExpires = DateTime.UtcNow.AddHours(_sourceSASExpireHours - 1);
            
            
            // Sleep for a sec to make sure it catches
            Thread.Sleep(1000);
        }
        #endregion


        static object _errorLockoObj = new object();
        private void LogStorageException(string message, StorageException ex)
        {
            // No exceptions from this method
            try
            {
                lock (_errorLockoObj)
                {
                    using (StreamWriter sw = new StreamWriter(StorageExceptionLogFile, true))
                    {
                        sw.WriteLine("{0}: {1}{2}{3}", DateTime.UtcNow, message, Environment.NewLine, ex);
                        sw.Flush();
                    }
                }
            }
            catch { }
        }


        private CloudBlobContainer GetContainer(ContainerType type)
        {
            CloudBlobContainer container = null;
            switch (type)
            {
                case ContainerType.SourceBlob:
                    container = _sourceBlobContainer;
                    break;
                case ContainerType.DestBlob:
                    container = _destBlobContainer;
                    break;
                case ContainerType.DestLog:
                    container = _destLogContainer;
                    break;
                default:
                    // N/A
                    break;
            }

            return container;
        }

    
        #endregion
    }

    public class CloudBlob
    {
        public ICloudBlob Blob { get; set; }
        public bool IsFromServer { get; set; }
    }
}
