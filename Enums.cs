﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
namespace Kfh.AzSync
{
    public enum PassType
    {
        Unknown,
        Partial,
        Full
    }
    
    public enum PassState
    {
        Unknown,
        InProgress,
        Completed
    }

    public enum ContainerType
    {
        SourceBlob,
        DestBlob,
        DestLog
    }

    public enum BlobAction
    {
        /// <summary>
        /// Source and Destination ContentMD5 hashes match. No Operation necessary
        /// </summary>
        NoOP,
        /// <summary>
        /// Blob does not exist in the destination container, add was requested
        /// </summary>
        Add,
        /// <summary>
        /// A previous copy request was sent, but did not complete. That request was aborted and a new one requested
        /// </summary>
        Repair,
        /// <summary>
        /// Source and Destination ContentMD5 hashes didn't match. A copy request has been sent to update the destination blob.
        /// </summary>
        Update,
        /// <summary>
        /// There is a pending operation we can't abort for some reason. We'll wait until next pass
        /// </summary>
        Pending,
        /// <summary>
        /// An error ocurred at some point during the processing.
        /// </summary>
        Error,
        /// <summary>
        /// A previous error was corrected on retry.
        /// </summary>
        ErrorCorrected
    }


}
