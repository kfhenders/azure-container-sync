﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;


namespace Kfh.AzSync
{
    public static class GZip
    {
        public static async Task CompressFileAsync(string uncompressedFile, string compressedFile)
        {
            using (FileStream uncompressedFs = new FileStream(uncompressedFile, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                using (FileStream compressedFs = new FileStream(compressedFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (GZipStream zipStream = new GZipStream(compressedFs, CompressionMode.Compress))
                    {
                        await uncompressedFs.CopyToAsync(zipStream).ConfigureAwait(false);
                    }
                }
            }
        }

        public static async Task DecompressFileAsync(string compressedFile, string uncompressedFile)
        {
            using (FileStream compressedFs = new FileStream(compressedFile, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                using (FileStream uncompressedFs = new FileStream(uncompressedFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (GZipStream zipStream = new GZipStream(compressedFs, CompressionMode.Decompress))
                    {
                        await zipStream.CopyToAsync(uncompressedFs).ConfigureAwait(false);
                    }
                }
            }
        }
    }
}
